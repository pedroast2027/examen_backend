﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using NET_CORE_API_1.Models;
using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Tutorial_NET_CORE_API_1.Data;
using Newtonsoft.Json.Serialization;

namespace NET_CORE_API_1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {

        public static User2 user2 = new User2();
        private SigningCredentials creds;
        
        private readonly GameContext _context;

        private IConfiguration config;

        public UserController(GameContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            List<User2> ListaUser;            //Id = User
            ListaUser = await _context.users2.Include(c => c.DetalleMago).ToListAsync(); //ToListAsync(quitar) nos dara todas las opciones 
            var jsonresponse = JsonParseLoopHandlingIgnore(ListaUser);

            //JsonConvert.SerializeObject(str, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });

            return Ok(jsonresponse);
           
            
            
            // return Ok(ListaUser);
            //var usr = await _context.users2.ToListAsync(); 
            //return Ok(usr);
        }

        private object JsonParseLoopHandlingIgnore(List<User2> listaUser)
        {
            DefaultContractResolver contractResolver = new DefaultContractResolver
            {
                NamingStrategy = new DefaultNamingStrategy()
            };
            var serializeSettings = new JsonSerializerSettings
            {
                ContractResolver = contractResolver,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };

            return JsonConvert.SerializeObject(listaUser, serializeSettings);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> GetById(int id)
        {
            var user = await _context.users2.FirstOrDefaultAsync(s => s.Id == id); 
            if (user == null) 
            {
                return NotFound();
            }

            return Ok(user);
        }

        [HttpPost("Register")]
        public async Task<ActionResult<User2>> Register(UserDto request)
        {
            CreatePasswordHash(request.Password, out byte[] passwordHash, out byte[] passwordSalt);

            user2.Username = request.Username;
            user2.PasswordHash = passwordHash;
            user2.PasswordSalt = passwordSalt;
            //user2.DetalleMagoId = request.MagoId;


            _context.users2.Add(user2);
            await _context.SaveChangesAsync();           
            return Ok(user2);

        }

        [HttpPost("Login")]

        public async Task<ActionResult<string>> Login2(UserDto request)
        {
            if(user2.Username != request.Username)
            {
                return BadRequest("User no found");
            }
            if(!VerifyPasswordHash(request.Password, user2.PasswordHash, user2.PasswordSalt))
            {
                return BadRequest("Wrong password.");
            }

            string token = CreateToken(user2);
            return Ok(token);

        }

        private string CreateToken(User2 user2)
        {

            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user2.Username)
            };

          //  var key = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(_configuration.GetSection("AppSettings:Token").Value));

            var cred = new SigningCreadentials(_context, SecurityAlgorithms.HmacSha512Signature);

            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: creds);

            var jwt = new JwtSecurityTokenHandler().WriteToken(token);

            return jwt;
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using(var hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }
        
        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                return computedHash.SequenceEqual(passwordHash);
            }
        }

    }
}
