﻿using Microsoft.IdentityModel.Tokens;
using Tutorial_NET_CORE_API_1.Data;

namespace NET_CORE_API_1.Controllers
{
    internal class SigningCreadentials
    {
        private SymmetricSecurityKey key;
        private string hmacSha512Signature;
        private GameContext context;

        public SigningCreadentials(SymmetricSecurityKey key, string hmacSha512Signature)
        {
            this.key = key;
            this.hmacSha512Signature = hmacSha512Signature;
        }

        public SigningCreadentials(GameContext context, string hmacSha512Signature)
        {
            this.context = context;
            this.hmacSha512Signature = hmacSha512Signature;
        }
    }
}