﻿
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tutorial_NET_CORE_API_1.Data;
using Tutorial_NET_CORE_API_1.Models;

namespace Tutorial_NET_CORE_API_1.Controllers
{
    [Route("api/[controller]")] //La rutra que va a tener por defecto este controlador para poder acceder
    [ApiController] //Le decimos que es un ApiController

    public class DetalleMagoController : ControllerBase
    {
        private readonly GameContext _context; //Para realizar una instancia tenemos que agregar esta propiedad llamando su contexto

        public DetalleMagoController(GameContext context)
        {
            _context = context;
        }


        [HttpGet]

        public async Task<ActionResult> GetAll() //Retornar todas las ordenes
        {
            //List<DetalleMago> ListaMago;            //Id = User
            //ListaMago = await _context.DetalleMagos.Include(c => c.Id).ToListAsync(); //ToListAsync(quitar) nos dara todas las opciones 
            //return Ok(ListaMago);

            var detalles = await _context.DetalleMagos.Include(b => b.users).ToListAsync();
            return Ok(detalles);
        }

        [HttpGet("{id}")] //Decir que va a recibir un id
        public async Task<ActionResult> GetById(int id) //Retornar una orden
        {
            var mago = await _context.DetalleMagos.FirstOrDefaultAsync(c => c.Id == id); //Este metodo dice: que si no encuuentra lo que estamos buscando nos vaa retornar un null
            if (mago == null)  // Aqui le decimos que si nos regresa null entonces imprima lo siguiente
            {
                return NotFound();
            }

            return Ok(mago);
        }


        [HttpGet("GetByMagoId/{magoId}")]
        public async Task<ActionResult> GetByOrderId(int MagoId)
        {
            var mago = await _context.DetalleMagos.FirstOrDefaultAsync(s => s.Id == MagoId);
            if (mago == null)
            {
                return NotFound();
            }

            return Ok(mago);
        }

        [HttpPost]
        public async Task<ActionResult> Post(DetalleMago detalle) //Agregar una orden
        {
         
            _context.DetalleMagos.Add(detalle);
            await _context.SaveChangesAsync(); //Guardar
            return CreatedAtAction(nameof(GetById), new { id = detalle.Id }, detalle);

        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, DetalleMago detalle)
        {
            if (id == detalle.Id)
            {
                _context.Entry(detalle).State = (Microsoft.EntityFrameworkCore.EntityState)EntityState.Modified;
                await _context.SaveChangesAsync(); //Guardar
                return NoContent();
            }

            return BadRequest();
        }



        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var detalle = await _context.DetalleMagos.FindAsync(id);
            if (detalle == null)
            {
                return NotFound();
            }
            _context.DetalleMagos.Remove(detalle);
            await _context.SaveChangesAsync();
            return NoContent();
        }


    }
}
