﻿using Microsoft.AspNetCore.Mvc;

namespace NET_CORE_API_1.Controllers
{
    public class TopController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
