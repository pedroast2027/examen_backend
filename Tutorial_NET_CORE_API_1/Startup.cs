using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using MVC_Tareas_segundo_plano.BackgroundsTask;
using System.Text;
using Tutorial_NET_CORE_API_1.Data;



namespace Tutorial_NET_CORE_API_1
{
    public class Startup
    {
 

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddDbContext<GameContext>(opt => opt.UseSqlServer(Configuration.GetConnectionString("Db_Game"))); //conexion a base de datos 
            
            //services.AddSingleton<IHostedService, IntervalTestBackgroundTask>(); //para ejecucion de (IntervalTaskHostService) en segundo plano }

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8
                        .GetBytes(Configuration.GetSection("AppSettings:Token").Value)),
                        ValidateIssuer = false,
                        ValidateAudience = false

                    };
                });



            //services.AddSingleton<IHostedService, IntervalTestBackgroundTask>();
            //services.AddDbContext<RestauranteContext>(opt => opt.UseInMemoryDatabase("RestauranteDb")); //Configurar una base de datos en memoria , durante se ejecute la aplicación
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) 
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseAuthentication();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
