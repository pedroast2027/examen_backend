﻿using Microsoft.EntityFrameworkCore;
using NET_CORE_API_1.Models;
using Tutorial_NET_CORE_API_1.Models;

namespace Tutorial_NET_CORE_API_1.Data
{
    public class GameContext : DbContext //Le decimos que va ederar de DbContext
    {

        public GameContext(DbContextOptions<GameContext> options)  //Agregamos un Constructor donde le decimos que DbContext utilice este conteto por defecto
             : base(options) { }
        public DbSet<DetalleMago> DetalleMagos { get; set; }
       // public DbSet<User> users { get; set; }
        public DbSet<User2> users2 { get; set; }
        public DbSet<TOP> top { get; set; }

    }
}
