﻿using NET_CORE_API_1.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Tutorial_NET_CORE_API_1.Models
{
    public class DetalleMago
    {
        [Key]
        public int Id { get; set; } 
        public string NameMago { get; set; }
        public int HitPoints { get; set; }
        public int Strength { get; set; }
        public int Defense { get; set; }
        public int Intelligence { get; set; }
        public int User2Id { get; set; }
        public virtual User2 users { get; set; }
       
    }
}
