﻿namespace NET_CORE_API_1.Models
{
    public class TOP
    {
        public int Id { get; set; } 
        public string NameTop { get; set; }
        public int TotalPower { get; set; }
    }
}
