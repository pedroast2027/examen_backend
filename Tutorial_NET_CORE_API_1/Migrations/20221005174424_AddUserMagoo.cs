﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NET_CORE_API_1.Migrations
{
    public partial class AddUserMagoo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "top",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NameTop = table.Column<string>(nullable: true),
                    TotalPower = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_top", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "users2",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Username = table.Column<string>(nullable: true),
                    PasswordHash = table.Column<byte[]>(nullable: true),
                    PasswordSalt = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_users2", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DetalleMagos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NameMago = table.Column<string>(nullable: true),
                    HitPoints = table.Column<int>(nullable: false),
                    Strength = table.Column<int>(nullable: false),
                    Defense = table.Column<int>(nullable: false),
                    Intelligence = table.Column<int>(nullable: false),
                    User2Id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DetalleMagos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DetalleMagos_users2_User2Id",
                        column: x => x.User2Id,
                        principalTable: "users2",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DetalleMagos_User2Id",
                table: "DetalleMagos",
                column: "User2Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DetalleMagos");

            migrationBuilder.DropTable(
                name: "top");

            migrationBuilder.DropTable(
                name: "users2");
        }
    }
}
