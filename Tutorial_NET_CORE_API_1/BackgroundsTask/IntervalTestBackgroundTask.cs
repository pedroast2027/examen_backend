﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using NET_CORE_API_1.Models;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Tutorial_NET_CORE_API_1.Data;

namespace MVC_Tareas_segundo_plano.BackgroundsTask
{
    public class IntervalTestBackgroundTask : BackgroundService
    {
        private readonly GameContext _context;

        public IntervalTestBackgroundTask(GameContext context)
        {
            _context = context;
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {

            var magosDb = await _context.DetalleMagos.ToListAsync();
            foreach(var mago in magosDb)
            {
                var total = mago.HitPoints + mago.Strength + mago.Defense + mago.Intelligence;

                TOP Top = new TOP()
                {

                    NameTop = mago.NameMago,
                    TotalPower = total
   
                };
                _context.Add(Top);
            }
        }
    }
}
